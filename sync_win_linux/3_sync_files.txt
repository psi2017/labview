# Connect
open sftp://psi2017:deepsleep@mu3esimon.dyndns.org
# Force binary mode transfer
option transfer binary
# Download file to the local directory d:\
synchronize remote E:\PSI17data /mnt/data/psi2017/preliminary/binary_files
# Disconnect
close
# Exit WinSCP
exit

